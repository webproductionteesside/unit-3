import { expect } from 'chai';
import 'mocha';
import { Wizard } from '../src/wizard';


describe('Weather Station constuction test', () => {
	it('It should be SUNNY', () => {
		const ws : WeatherStation = new WeatherStation();

		expect(ws.get()).to.equal(EWeather.SUNNY);
	});
});

describe('Weather Station set() test', () => {
	it('It should be SNOWY', () => {
		const ws : WeatherStation = new WeatherStation();
		ws.set(EWeather.SNOWY);

		expect(ws.get()).to.equal(EWeather.SNOWY);
	});
});
