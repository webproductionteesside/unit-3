import { Wizard } from "./wizard";

const wizzy: Wizard = new Wizard("Wizzy", 45, 6);

wizzy.fight();
wizzy.fight();
wizzy.cast();
wizzy.meditate();
wizzy.cast();
