import { EWizardState } from "./states";

export class Wizard {
  private state_: EWizardState = EWizardState.NORMAL;

  constructor(
    private name: string,
    private hitPoints: number,
    private strength: number
  ) {

  }

  set state(ws: EWizardState) {
    this.state_ = ws;
  }

  get state(): EWizardState {
    return this.state_;
  }

  public fight(): void {
    switch(this.state) {
      case EWizardState.NORMAL: {
        console.log("You swing with the force of 10 bears");
        break;​
      }
      case EWizardState.FOCUSED: {
        console.log("You're still in a trance!");
        break;​
      }
      case EWizardState.FATIGUED: {
        console.log("You make a feeble lunge");​
        break;
      }
    }
  }

  public cast(): void {
    switch(this.state) {
      case EWizardState.NORMAL: {
        console.log("You realise you have to meditate");
        break;​
      }
      case EWizardState.FOCUSED: {
        console.log("You unleash magical terror and imps!");
        this.state = EWizardState.FATIGUED;​
        break;​
      }
      case EWizardState.FATIGUED: {
        console.log("You realise that you are exhausted");​
        break;
      }
    }
  }

  public sleep() {
    console.log("You sleep for 6 hours and awake refreshed.");
    this.state = EWizardState.NORMAL;
  }

  public meditate() {
    console.log("You chant some stuff and make omm noises");
    this.state = EWizardState.FOCUSED;
  }
}
