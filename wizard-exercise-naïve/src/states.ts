export enum EWizardState {
  NORMAL, FOCUSED, FATIGUED
}
